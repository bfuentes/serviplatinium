-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 06, 2017 at 08:02 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `serviplatinium_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(2) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `banner`
--

TRUNCATE TABLE `banner`;
--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `nombre`, `descripcion`, `imagen`, `estado`) VALUES
(1, 'Aveo Emotion Advance', 'Nuevo modelo 2011', 'images/aveo-emotion-advance-2016-alquiler-en-guayaquil-ecuador.jpg', 1),
(2, 'Kia Revolution', 'Nuevo modelo 2011', 'images/promo_kia_rio_xcite.jpg', 0),
(3, 'Chevrolet Aveo Family', '', 'images/alquiler-aveo-family-2016-en-guayaquil.jpg', 1),
(4, 'Aveo Emotion 2016', '', 'images/aveo-emotion-2016-alquiler-en-guayaquil-ecuador.jpg', 1),
(5, 'Kia Rio Revolution', '', 'images/alquiler-de-auto-sedan-kia-rio-revolution.jpg', 1),
(6, 'Hyundai Tucson ix', '', 'images/hyundai-tucson-ix-de-alquiler-en-guayaquil.jpg', 1),
(7, 'Suzuki Sz', '', 'images/alquiler-de-suzuki-sz-en-guayaquil.jpg', 1),
(8, 'Kia Sportage Active', '', 'images/alquiler-de-kia-sportage-active-en-guayaquil.jpg', 1),
(9, 'Hyundai Santa FÃ©', '', 'images/img_banner_santa_fe.jpg', 0),
(10, 'Hyundai H1', '', 'images/alquiler-de-furgoneta-hyundai-h1-en-ecuador.jpg', 1),
(11, 'SUZUKI SZ AUTOMATICO 4X2 FULL', '', 'images/sz plomo.jpg', 0),
(12, 'Chevrolet Sail', '', 'images/SAIL SEDAN.jpg', 0),
(13, 'GRAND VITARA 4X2  ', '', 'images/GRAND VITARA.jpg', 0),
(14, 'New Accent', '', 'images/new_accent.jpg', 0),
(15, 'Orlando', '', 'images/orlando.jpg', 0),
(16, 'chevrolet Emotion sencillo', '', 'images/emotion nuevo 2.jpg', 0),
(17, 'Chevrolet Orlando', '', 'images/alquiler-de-auto-chevrolet-orlando-en-guayaquil.jpg', 1),
(18, 'Chevrolet Dmax 4x2 Diesel', '', 'images/alquiler-de-camioneta-chevrolet-dmax-4x2-diesel.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE `categorias` (
  `id` int(1) NOT NULL,
  `categoria` varchar(50) CHARACTER SET utf8 NOT NULL,
  `estado` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `categorias`
--

TRUNCATE TABLE `categorias`;
--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`, `estado`) VALUES
(1, '', 1),
(2, 'Autos', 1),
(3, 'Camionetas', 1),
(4, 'Suvs', 1),
(5, 'furgonetas', 1);

-- --------------------------------------------------------

--
-- Table structure for table `informacion`
--

CREATE TABLE `informacion` (
  `direccion_esp` varchar(500) CHARACTER SET utf8 NOT NULL,
  `telefono_01` varchar(50) CHARACTER SET utf8 NOT NULL,
  `telefono_02` varchar(50) CHARACTER SET utf8 NOT NULL,
  `telefono_03` varchar(50) CHARACTER SET utf8 NOT NULL,
  `telefono_04` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `informacion`
--

TRUNCATE TABLE `informacion`;
--
-- Dumping data for table `informacion`
--

INSERT INTO `informacion` (`direccion_esp`, `telefono_01`, `telefono_02`, `telefono_03`, `telefono_04`) VALUES
('<p>Av.Hermano Miguel, cdla. Sim&oacute;n Bolivar Mz. 2 villa 75 local #1, frente a Frenar.</p>', '(593)042924727', '(593)0994795665', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mapas`
--

CREATE TABLE `mapas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) CHARACTER SET utf8 NOT NULL,
  `imagen` varchar(250) CHARACTER SET utf8 NOT NULL,
  `estado` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `mapas`
--

TRUNCATE TABLE `mapas`;
--
-- Dumping data for table `mapas`
--

INSERT INTO `mapas` (`id`, `nombre`, `imagen`, `estado`) VALUES
(1, 'Guayaquil (malecÃ³n 2000)', 'images/guayaquil.jpg', 1),
(2, 'Guayaquil (Aeropuerto)', 'images/aeropuerto.jpg', 1),
(3, 'Atacames', 'images/atacames.jpg', 1),
(4, 'BaÃ±os', 'images/banos.jpg', 1),
(5, 'Cotopaxi', 'images/cotopaxi.jpg', 1),
(6, 'Mompiche', 'images/mompiche.jpg', 1),
(7, 'MontaÃ±ita', 'images/montanita.jpg', 1),
(8, 'Salinas', 'images/salinas.jpg', 1),
(9, 'Quito (Mitad del Mundo)', 'images/quito.jpg', 1),
(10, 'Tungurahua', 'images/tungurahua.jpg', 1),
(11, 'Tena', 'images/oriente.jpg', 1),
(12, 'Guayaquil (Puerto Santa Ana)', 'images/puerto-santa-ana-guayaquil.jpg', 1),
(13, 'Guayaquil (Las PeÃ±as)', 'images/el-faro-guayaquil.jpg', 1),
(14, 'Guayaquil (La Perla)', 'images/la-perla-guayaquil.jpg', 1),
(15, 'BaÃ±os de Agua Santa', 'images/baÃ±os-columpio.jpg', 1),
(16, 'Cuenca', 'images/cuenca_plaza_central_inmaculada_concepcion_cathedral.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `promociones`
--

CREATE TABLE `promociones` (
  `id` int(10) NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 NOT NULL,
  `descripcion` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `imagen` varchar(250) CHARACTER SET utf8 NOT NULL,
  `precio` int(10) NOT NULL,
  `precio_promo` int(10) NOT NULL,
  `estado` int(1) NOT NULL,
  `dias` int(2) NOT NULL,
  `categoria` varchar(20) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `promociones`
--

TRUNCATE TABLE `promociones`;
--
-- Dumping data for table `promociones`
--

INSERT INTO `promociones` (`id`, `nombre`, `descripcion`, `imagen`, `precio`, `precio_promo`, `estado`, `dias`, `categoria`) VALUES
(21, 'HYUNDAI H1', '', 'images/hyundai-h1-.jpg', 910, 875, 1, 7, 'Furgonetas'),
(20, 'MAZDA BT-50 4X4', '', 'images/promo_mazda_4x4.jpg', 714, 616, 0, 7, 'Camionetas'),
(19, 'MAZDA BT-50 4X2', '', 'images/promo_mazda_4x2.jpg', 750, 700, 0, 10, 'Camionetas'),
(18, 'CHEVROLET DMAX 4X4', '', 'images/promo_chevrolet_dmax_4x4.jpg', 714, 616, 0, 7, 'Camionetas'),
(13, 'KIA SPORTAGE ACTIVO', '', 'images/kia_sportage_blanco.jpg', 560, 525, 1, 7, 'Suvs'),
(14, 'SUZUKI SZ', '', 'images/promo_suzuki_sz.jpg', 595, 560, 1, 7, 'Suvs'),
(15, 'HYUNDAI TUCSON IX', '', 'images/promo_hyundai_tucson_ix.jpg', 665, 630, 1, 7, 'Suvs'),
(16, 'SUZUKI SZ AUTOMATICO 4X2 FULL', '', 'images/SZ_2011.jpg', 850, 680, 0, 10, 'Suvs'),
(17, 'CHEVROLET DMAX 4X2', '', 'images/promo_chevrolet_dmax_4x2.jpg', 595, 560, 1, 7, 'Camionetas'),
(10, 'CHEVROLET AVEO EMOTION', '', 'images/promo_aveo_activo.jpg', 385, 350, 1, 7, 'Autos'),
(11, 'KIA RIO REVOLUTION', '', 'images/kia-rio-revolution.jpg', 420, 385, 1, 7, 'Autos'),
(12, 'AVEO EMOTION ADVANCE', '', 'images/aveo_emotion_advance.jpg', 420, 385, 1, 7, 'Autos'),
(9, 'CHEVROLET AVEO FAMILY', '', 'images/aveo-family.jpg', 315, 280, 1, 7, 'Autos'),
(22, 'TOYOTA HI LUX', '', 'images/Toyota-Hilux.jpg', 667, 627, 0, 7, 'Camionetas'),
(24, 'CHEVROLET ORLANDO', '', 'images/chevrolet-orlando.jpg', 735, 700, 1, 7, 'Suvs'),
(23, 'GRAND VITARA', '', 'images/grand_vitara.jpg', 490, 455, 1, 7, 'Suvs');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `contrasenia` varchar(45) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `estado` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `usuario`
--

TRUNCATE TABLE `usuario`;
--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `usuario`, `contrasenia`, `tipo`, `estado`) VALUES
(1, 'Administrador', 'admin', 'serviplatinium', 'Administrador', 1),
(2, 'webmaster', 'webmaster', 'serviplatinium', 'Administrador', 1),
(3, 'Administrador', 'administrador', 'jesusesmipastor', 'Administrador', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehiculos`
--

CREATE TABLE `vehiculos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 NOT NULL,
  `descripcion_esp` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `descripcion_eng` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `imagen` varchar(250) CHARACTER SET utf8 NOT NULL,
  `precio_dia` int(10) NOT NULL,
  `categoria` varchar(250) CHARACTER SET utf8 NOT NULL,
  `estado` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `vehiculos`
--

TRUNCATE TABLE `vehiculos`;
--
-- Dumping data for table `vehiculos`
--

INSERT INTO `vehiculos` (`id`, `nombre`, `descripcion_esp`, `descripcion_eng`, `imagen`, `precio_dia`, `categoria`, `estado`) VALUES
(21, 'CHEVROLET AVEO EMOTION', '<p><span style=\"font-size: 8pt; font-family: Tahoma;\">Puedes elegir entre las dos versiones de equipamiento disponible de Chevrolet Aveo @ctivo Sed&aacute;n 4 Puertas: STD y AC. Ambos tienen direcci&oacute;n hidr&aacute;ulica; elevavidrios el&eacute;ctricos delanteros; cierre centralizado y equipo de sonido con cd y 4 parlantes. La versi&oacute;n AC incorpora, adem&aacute;s, aire acondicionado.DESDE</span></p>', '<p><span id=\"result_box\" lang=\"en\"><span>You can choose between the two versions of equipment available for Chevrolet Aveo 4-Door Sedan @ ctive: STD and AC. </span><span>Both have power steering, electric front elevavidrios, central locking and stereo with CD and 4 speakers. </span><span>AC version also incorporates air conditioning.</span></span></p>', 'images/aveo_activo.jpg', 55, 'Autos', 1),
(20, 'CHEVROLET AVEO FAMILY', '<p>En materia de equipamiento, el Chevrolet Aveo Family incorpora direcci&oacute;n hidr&aacute;ulica; aire acondicionado (versi&oacute;n A/C); equipo de sonido con cd, mp3 y entrada auxiliar marca Kenwood con 4 parlantes; asientos posteriores abatibles 60/40 con apoyacabezas; consola central con compartimiento, portavaso trasero y doble portavaso delantero; reloj digital y apertura de ba&uacute;l y de tanque de combustible desde el interior del veh&iacute;culo, entre otros destacados. DESDE</p>', '<p><span id=\"result_box\" lang=\"en\"><span>In terms of equipment, the Chevrolet Aveo Family includes power steering, air conditioning (version A / C), stereo with CD, MP3 and auxiliary input with 4 Kenwood speakers, 60/40 folding rear seats with headrests, center console </span><span>bin, rear cup holder and dual front cup holders, digital clock and trunk release and fuel tank from inside the vehicle, among other highlights.</span></span></p>', 'images/aveo-family.jpg', 45, 'Autos', 1),
(22, 'AVEO EMOTION ADVANCE', '<ul>\r\n<li>5 Puertas</li>\r\n<li>Aire Acondiocionado</li>\r\n<li>Capacidad para 5 personas</li>\r\n<li>Tipo HACHBACK .DESDE</li>\r\n</ul>', '<ul>\r\n<li>5 Doors</li>\r\n<li>Air Conditioning</li>\r\n<li>Capacity 5 people</li>\r\n<li>Type HACHBACK</li>\r\n</ul>', 'images/aveo_emotion_advance.jpg', 60, 'Autos', 1),
(23, 'KIA RIO REVOLUTION', '<ul>\r\n<li>4 Puertas</li>\r\n<li>Aire Acondiocionado</li>\r\n<li>Capacidad para 5 personas</li>\r\n<li>Tipo SEDAN</li>\r\n</ul>', '<ul>\r\n<li>4 Doors</li>\r\n<li>Air Conditioning</li>\r\n<li>Capacity 5 people</li>\r\n<li>Type SEDAN</li>\r\n</ul>', 'images/kia-rio-revolution.jpg', 60, 'Autos', 1),
(24, 'MAZDA BT-50 4X4', '<ul>\r\n<li>4 Puertas</li>\r\n<li>Aire Acondiocionado</li>\r\n<li>Capacidad para 5 personas</li>\r\n<li>Tipo 4X4 .DESDE</li>\r\n</ul>', '<ul>\r\n<li>4 Doors</li>\r\n<li>Air Conditioning</li>\r\n<li>Capacity 5 people</li>\r\n<li>Type 4X4</li>\r\n</ul>', 'images/mazda_4x4.jpg', 89, 'Camionetas', 0),
(25, 'MAZDA BT-50 4X2', '<ul>\r\n<li>4 Puertas</li>\r\n<li>Aire Acondiocionado</li>\r\n<li>Capacidad para 5 personas</li>\r\n<li>Tipo 4X2.DESDE</li>\r\n</ul>', '<ul>\r\n<li>4 Doors</li>\r\n<li>Air Conditioning</li>\r\n<li>Capacity 5 people</li>\r\n<li>Type 4X2</li>\r\n</ul>', 'images/mazda_4x2.jpg', 75, 'Camionetas', 0),
(26, 'CHEVROLET DMAX 4X4', '<ul>\r\n<li>4 Puertas</li>\r\n<li>Aire Acondiocionado</li>\r\n<li>Capacidad para 5 personas</li>\r\n<li>Tipo 4X4. DESDE</li>\r\n</ul>', '<ul>\r\n<li>4 Doors</li>\r\n<li>Air Conditioning</li>\r\n<li>Capacity 5 people</li>\r\n<li>Type 4X4</li>\r\n</ul>', 'images/chevrolet_dmax_4x4.jpg', 89, 'Camionetas', 0),
(27, 'CHEVROLET DMAX 4X2', '<ul>\r\n<li>Motor a Diesel</li>\r\n<li>4 Puertas</li>\r\n<li>Aire Acondiocionado</li>\r\n<li>Capacidad para 5 personas</li>\r\n<li>Tipo 4X2 .DESDE</li>\r\n</ul>', '<ul>\r\n<li>Diesel Motor</li>\r\n<li>4 Doors</li>\r\n<li>Air Conditioning</li>\r\n<li>Capacity 5 people</li>\r\n<li>Type 4X2</li>\r\n</ul>', 'images/chevrolet_dmax_4x2.jpg', 85, 'Camionetas', 1),
(28, 'KIA SPORTAGE ACTIVO', '<ul>\r\n<li>5 Puertas</li>\r\n<li>Aire Acondiocionado</li>\r\n<li>Capacidad para 5 personas</li>\r\n<li>Tipo SUV.DESDE</li>\r\n</ul>', '<ul>\r\n<li>5 Doors</li>\r\n<li>Air Conditioning</li>\r\n<li>Capacity 5 people</li>\r\n<li>Type SUV</li>\r\n</ul>', 'images/kia_sportage_blanco.jpg', 80, 'Suvs', 1),
(29, 'SUZUKI SZ', '<ul>\r\n<li>5 Puertas</li>\r\n<li>Aire Acondiocionado</li>\r\n<li>Capacidad para 5 personas</li>\r\n<li>Tipo SUV.DESDE</li>\r\n</ul>', '<ul>\r\n<li>5 Doors</li>\r\n<li>Air Conditioning</li>\r\n<li>Capacity 5 people</li>\r\n<li>Type SUV</li>\r\n</ul>', 'images/suzuki_sz.jpg', 85, 'Suvs', 1),
(30, 'HYUNDAI TUCSON IX', '<ul>\r\n<li>5 Puertas</li>\r\n<li>Aire Acondiocionado</li>\r\n<li>Capacidad para 5 personas</li>\r\n<li>Tipo SUV 4X2</li>\r\n<li>AUTOMATICO</li>\r\n<li>DEL A&Ntilde;O</li>\r\n</ul>', '<ul>\r\n<li>5 Doors</li>\r\n<li>Air Conditioning</li>\r\n<li>Capacity 5 people</li>\r\n<li>Type SUV</li>\r\n<li>A T</li>\r\n<li>2012</li>\r\n</ul>', 'images/hyundai_tucson_ix.jpg', 95, 'Suvs', 1),
(31, 'SUZUKI SZ AUTOMATICO 4X2 ', '<ul>\r\n<li>5 Puertas</li>\r\n<li>Aire Acondiocionado</li>\r\n<li>Capacidad para 5 personas</li>\r\n<li>Tipo SUV.</li>\r\n<li>AUTOMATICO</li>\r\n</ul>', '<ul>\r\n<li>5 Doors</li>\r\n<li>Air Conditioning</li>\r\n<li>Capacity 5 people</li>\r\n<li>Type SUV</li>\r\n<li>A T</li>\r\n</ul>', 'images/sz rojo.jpg', 80, 'Suvs', 0),
(32, 'HYUNDAI H1', '<ul>\r\n<li>5 Puertas</li>\r\n<li>&nbsp;2 Aire Acondiocionado</li>\r\n<li>Capacidad para 12 personas</li>\r\n<li>Tipo furgoneta van desde </li>\r\n</ul>', '<ul>\r\n<li>5 Doors</li>\r\n<li>Air Conditioning</li>\r\n<li>Capacity&nbsp; 12&nbsp;people</li>\r\n<li>Type&nbsp; van </li>\r\n</ul>', 'images/hyundai-h1-.jpg', 130, 'Furgonetas', 1),
(33, 'GRAND VITARA', '<ul>\r\n<li>VEHICULO CON CHASIS</li>\r\n<li>4X2</li>\r\n<li>AIRE ACONDICIONADO</li>\r\n<li>5 PASAJEROS</li>\r\n<li>FULL EQUIPO</li>\r\n<li>TRANSMISION MECANICA</li>\r\n</ul>', '', 'images/grand_vitara.jpg', 70, 'Suvs', 1),
(34, 'CHEVROLET ORLANDO', '', '', 'images/chevrolet-orlando.jpg', 105, 'Suvs', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `mapas`
--
ALTER TABLE `mapas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promociones`
--
ALTER TABLE `promociones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `mapas`
--
ALTER TABLE `mapas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `promociones`
--
ALTER TABLE `promociones`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
