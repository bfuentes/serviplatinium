﻿<?php
	ob_start();
	session_start();
	require 'ctrl_cookie.php';
	ob_flush();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/administrador.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- InstanceBeginEditable name="doctitle" -->
<title>Serviplatinium RENT A CAR Alquiler de autos Guayaquil Ecuador a pocos metros del Aeropuerto de Guayaquil José Joaquín de Olmedo</title>
<!-- InstanceEndEditable -->

<link href="../css/body_interno.css" rel="stylesheet" type="text/css" />
<link href="../css/diseno.css" rel="stylesheet" type="text/css" />
<link href="../css/texto.css" rel="stylesheet" type="text/css" />

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

</head>
<body onload=ocultar();>


<div id="contenedor_global"><!-- 0 -->

<div id="contenedor_idioma"><!-- 0.1 -->
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr>
      <td class="texto_mini_gris">Idioma</td>
      <td><img src="../images/separador.png" width="4" height="22" /></td>
      <td><a href="../index.php"><img src="../images/idioma_esp.png" width="29" height="22" border="0" /></a></td>
      <td><a href="../website_eng/index.php"><img src="../images/idioma_eng.png" width="29" height="22" border="0" /></a></td>
    </tr>
  </table>
</div><!--fin 10.1 -->



<div id="contenedor_cabecera"><!-- 1 -->

	
    <div id="contenedor_logotipo"><!-- 1.1 -->
    <a href="http://www.serviplatiniumrentacar.com"><img src="../images/logo_serviplatinium.png" width="340" height="123" border="0" /></a></div><!--fin 1.1 -->

	<div id="contenedor_botonera"><!-- 1.2 -->
	  <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="560" height="90">
	    <param name="movie" value="../swf/botonera_serviplatinium.swf" />
	    <param name="quality" value="high" />
	    <param name="wmode" value="transparent" />
	    <param name="swfversion" value="6.0.65.0" />
	    <!-- Esta etiqueta param indica a los usuarios de Flash Player 6.0 r65 o posterior que descarguen la versión más reciente de Flash Player. Elimínela si no desea que los usuarios vean el mensaje. -->
	    <param name="expressinstall" value="../Scripts/expressInstall.swf" />
	    <!-- La siguiente etiqueta object es para navegadores distintos de IE. Ocúltela a IE mediante IECC. -->
	    <!--[if !IE]>-->
	    <object type="application/x-shockwave-flash" data="../swf/botonera_serviplatinium.swf" width="560" height="90">
	      <!--<![endif]-->
	      <param name="quality" value="high" />
	      <param name="wmode" value="transparent" />
	      <param name="swfversion" value="6.0.65.0" />
	      <param name="expressinstall" value="../Scripts/expressInstall.swf" />
	      <!-- El navegador muestra el siguiente contenido alternativo para usuarios con Flash Player 6.0 o versiones anteriores. -->
	      <div>
	        <h4>El contenido de esta página requiere una versión más reciente de Adobe Flash Player.</h4>
	        <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Obtener Adobe Flash Player" width="112" height="33" /></a></p>
          </div>
	      <!--[if !IE]>-->
        </object>
	    <!--<![endif]-->
      </object>
    </div><!--fin 1.2 -->
    
</div>
<!--fin 1 -->
<!--fin 2 -->





<div id="contenedor_cuerpo_info"><!-- 3 -->

	<div id="contenedor_info_sec_01_admin"><!-- 3.1 -->

		<div id="contenedor_publicidades"><!-- 3.1.2 --><!--fin 3.1.2.1 --><!--fin 3.1.2.0 --><!--fin 3.1.2.2 -->
		  <table width="100%" border="0" cellspacing="10" cellpadding="0">
		    <tr>
		      <td height="40" align="center" class="titulo_blanco">Administrador</td>
	        </tr>
		    <tr>
		      <td><div class="linea_gr" id="contenedor_pub">
	            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		          <tr>
		            <td width="14%"><img src="../images/ico_banner.png" width="30" height="30" /></td>
		            <td width="86%" class="texto_menu_administracion"><a href="banner_admin/reporte.php" class="enlaces_gris">Banner principal</a></td>
	              </tr>
	            </table>
		        </div>
		        <div id="contenedor_pub_separador">
		          <!-- 3.1.2.0 -->
	            </div>
                
                
		        <div class="linea_gr" id="contenedor_pub">
		          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="8">
		            <tr>
		              <td width="14%"><img src="../images/ico_info.png" width="30" height="30" /></td>
		              <td width="86%" class="texto_menu_administracion"><a href="informacion_admin/reporte.php" class="enlaces_gris">Información General</a></td>
	                </tr>
	              </table>
	            </div>
		        <div id="contenedor_pub_separador">
		          <!-- 3.1.2.0 -->
	            </div>
		        <div  id="contenedor_pub">
		          <table width="99%" border="0" align="center" cellpadding="0" cellspacing="8">
		            <tr>
		              <td width="14%"><img src="../images/ico_tipo - Copy.png" width="30" height="30" /></td>
		              <td width="86%"><a href="vehiculos_admin/reporte_vehiculos.php" class="enlaces_gris">Nuestros Vehículos</a></td>
	                </tr>
	              </table>
	            </div>
		        <div id="contenedor_pub_separador">
		          <!-- 3.1.2.0 -->
	            </div>
		        <div id="contenedor_pub">
		          <table width="99%" border="0" align="center" cellpadding="0" cellspacing="8">
		            <tr>
		              <td width="14%"><img src="../images/ico_promociones.png" width="30" height="30" /></td>
		              <td width="86%" class="texto_gr_link"><a href="promociones_admin/reporte_promociones.php" class="enlaces_gris">Promociones</a></td>
	                </tr>
	              </table>
	            </div>
		        <div id="contenedor_pub_separador">
		          <!-- 3.1.2.0 -->
	            </div>
                
                <div id="contenedor_pub">
		          <table width="99%" border="0" align="center" cellpadding="0" cellspacing="8">
		            <tr>
		              <td width="14%"><img src="../images/ico_vehiculos.png" width="30" height="30" /></td>
		              <td width="86%" class="texto_gr_link"><a href="mapas_admin/reporte.php" class="enlaces_gris">Guia Turística</a></td>
	                </tr>
	              </table>
	            </div>
		        <div id="contenedor_pub_separador">
		          <!-- 3.1.2.0 -->
	            </div>
                
                
		        <div id="contenedor_pub"">
		          <table width="99%" border="0" align="center" cellpadding="0" cellspacing="8">
		            <tr>
		              <td width="14%"><img src="../images/ico_salir.png" width="30" height="30" /></td>
		              <td width="86%" class="texto_gr_link"><a href="ctrl_cerrar_sesion.php" class="enlaces_gris">Salir</a></td>
	                </tr>
	              </table>
	            </div></td>
	        </tr>
	      </table>
  </div><!--fin 3.1.2 -->
        
		<div id="contenedor_info_admin"><!-- 3.1.1 -->
    <!-- InstanceBeginEditable name="informacion" -->
    <br /><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
	      <tr>
	        <td height="450" align="center" valign="middle"><img src="../images/img_admin.jpg" width="383" height="221" /></td>
        </tr>
      </table>
    <!-- InstanceEndEditable --></div>
		<!--fin 3.1.1 --></div><!--fin 3.1 --><!--fin 3.2 -->
    
  
    
</div><!--fin 3 -->

<div id="contenedor_pie"><!-- 4 -->
    
    	<div id="contenedor_derechos"><!-- 4.1 -->
    	  <table width="430" border="0" cellpadding="0" cellspacing="5">
    	    <tr>
    	      <td width="100" class="texto_pie_pagina"><p><a href="http://www.xtudiocreativo.com"><img src="../images/logo_xtudiocreativo.png" name="xtudiocreativo_publicidad_ecuador" width="100" height="17" border="0" id="xtudiocreativo_publicidad_ecuador" /></a></p></td>
    	      <td width="24" align="center" class="texto_pie_pagina"><img src="../images/separador.png" width="2" height="11" /></td>
    	      <td width="286" class="texto_mini_gris">Sitio web Serviplatinium rent a car<br />
   	          Dise&ntilde;ado y producido por <a href="http://www.xtudiocreativo.com" target="_blank" class="enlaces_mini_gris">xtudiocreativo</a> / 2010</td>
  	      </tr>
  	    </table>
   	</div><!--fin 4.1 -->
        
    	<div id="contenedor_enlaces_pie"><!-- 4.2 -->
    	  <table width="100%" border="0" cellspacing="15" cellpadding="0">
    	    <tr>
    	      <td class="texto_mini_gris"><a href="../index.php" class="enlaces_mini_gris">Inicio</a></td>
    	      <td><img src="../images/separador.png" name="alquier" width="2" height="11" id="alquier" /></td>
    	      <td><a href="../promociones.php" class="enlaces_mini_gris">Promociones</a></td>
    	      <td><img src="../images/separador.png" name="autos" width="2" height="11" id="autos" /></td>
    	      <td><a href="../renta_de_autos_ecuador.php" class="enlaces_mini_gris">Vehículos</a></td>
    	      <td><img src="../images/separador.png" name="renta_autos" width="2" height="11" id="renta_autos" /></td>
    	      <td><a href="../contactos.php" class="enlaces_mini_gris">Contáctenos</a></td>
  	      </tr>
  	    </table>
    	</div><!--fin 4.2 -->
    
  </div><!--fin 4 -->


</div>
<!--fin 0 -->




<script type="text/javascript">
<!--
swfobject.registerObject("FlashID");
//-->
</script>
</body>
<!-- InstanceEnd --></html>
