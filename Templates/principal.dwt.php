<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- TemplateBeginEditable name="doctitle" -->
<title>Serviplatinium RENT A CAR Alquiler de autos Guayaquil Ecuador a pocos metros del Aeropuerto de Guayaquil José Joaquín de Olmedo</title>
<!-- TemplateEndEditable -->
<link href="../css/diseno.css" rel="stylesheet" type="text/css" />
<link href="../css/texto.css" rel="stylesheet" type="text/css" />

<!-- GOOGLE -->
<meta name="robots" content="all" />
<meta name="google-site-verification" content="tjlvNw2xhQzdKMF2Ob1KI_wdCvjHuIm50Dy3-2MMAmQ" />

<script src="../Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19248298-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<!-- GOOGLE XTUDIOCREATIVO -->
<script type="text/javascript">
var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19248099-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>

<!-- ALEXA -->
<meta name="alexaVerifyID" content="dFz9QZ_XVf9m20op1zLonuxE0kk" />

<!-- LINKS -->
<link href="http://www.xtudiocreativo.com/" />
<link href="http://www.alexa.com/" />
<LINK REV=made href="mailto:info@xtudiocreativo.com">

<meta name="title" content="Serviplatinium Rent a Car, alquiler de vehiculos, a pocos metros del Aeropuerto de Guayaquil, renta de autos, Car Rental, Ecuador, rental cars, rental vehicles, cars, vans">

<meta name="author" content="Xtudiocreativo WG">

<meta name="keywords" content="Rent a Car, Alquiler de vehiculos Ecuador, Guayaquil, Aeropuerto de guayaquil José Joaquin de Olmedo, Av de las Americas, renta de autos, autos de alquiler, renta de vehiculos, autos, furgonetas, Car Rental, Guayaquil's Airport José Joaquin de Olmedo, rental cars, rental cars, rental vehicles, cars, vans, alquiler de vehiculos guayaquil ecuador, alquiler de autos guayaquil ecuador, alquiler de automoviles guayaquil ecuador, alquiler de coches ecuador guayaqui, renta de autos guayaquil ecuador, renta de vehiculos guayaquil ecuador, renta de automoviles guayaquil ecuador, renta de coches guayaquil, alquiler vehiculos automaticos guayaquil quito ecuador ">

<meta name="description" content="Serviplatinium Rent a Car, alquiler de vehiculos, ubicada a pocos metros del Aeropuerto de Guayaquil José Joaquin de Olmedo, alquiler de autos en excelentes condiciones, añadale toda la seguridad y confort a sus viajes cosultando nuestras promociones, Serviplatinium Rent a Car">

<meta name="DISTRIBUTION" content="Global"></meta>


<!-- CODE FOR BANNERS -->
<!-- Use the ID of your slider here to avoid the flash of unstyled content -->
	  	<style type="text/css">
	  		#featured {
	width: 644px;
	height: 225px;
	background: #009cff url('../images/loading.gif') no-repeat center center;
	overflow: hidden;
}
	  		/* Want a different Loading GIF - visit http://www.webscriptlab.com/ - that's where we go this one :) */
	  	</style>
		
		<!-- Attach our CSS -->
<link rel="stylesheet" href="../css/orbit.css">	
	  	
		<!--[if IE]>
			<style type="text/css">
				.timer { display: none !important; }
				div.caption { background:transparent; filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000,endColorstr=#99000000);zoom: 1; }
			</style>
		<![endif]-->
	  	
		<!-- Attach necessary scripts -->
		<script type="text/javascript" src="../js/jquery-1.4.1.min.js"></script>
		<script type="text/javascript" src="../js/jquery.orbit.min.js"></script>
		
		<!-- Run the plugin -->
		<script type="text/javascript">
			$(window).load(function() {
				$('#featured').orbit({
					'bullets': true,
					'timer' : true,
					'animation' : 'horizontal-slide'
				});
			});
		</script>

<!-- FIN CODE FOR BANNERS -->


<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
</head>
<body onload="MM_preloadImages('../images/btnv_chat_ov.png','../images/btnv_guia_ov.png','../images/btnv_cont_ov.png')">


<div id="contenedor_global"><!-- 0 -->

<div id="contenedor_idioma"><!-- 0.1 -->
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr>
      <td class="texto_mini_gris">Idioma</td>
      <td><img src="../images/separador.png" width="4" height="22" /></td>
      <td><img src="../images/idioma_esp.png" width="29" height="22" /></td>
      <td><img src="../images/idioma_eng.png" width="29" height="22" /></td>
    </tr>
  </table>
</div><!--fin 10.1 -->



<div id="contenedor_cabecera"><!-- 1 -->

	
    <div id="contenedor_logotipo"><!-- 1.1 -->
    <a href="http://www.serviplatiniumrentacar.com"><img src="../images/logo_serviplatinium.png" width="340" height="123" border="0" /></a></div><!--fin 1.1 -->

	<div id="contenedor_botonera"><!-- 1.2 -->
	  <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="560" height="90">
	    <param name="movie" value="../swf/botonera_serviplatinium.swf" />
	    <param name="quality" value="high" />
	    <param name="wmode" value="transparent" />
	    <param name="swfversion" value="6.0.65.0" />
	    <!-- Esta etiqueta param indica a los usuarios de Flash Player 6.0 r65 o posterior que descarguen la versión más reciente de Flash Player. Elimínela si no desea que los usuarios vean el mensaje. -->
	    <param name="expressinstall" value="../Scripts/expressInstall.swf" />
	    <!-- La siguiente etiqueta object es para navegadores distintos de IE. Ocúltela a IE mediante IECC. -->
	    <!--[if !IE]>-->
	    <object type="application/x-shockwave-flash" data="../swf/botonera_serviplatinium.swf" width="560" height="90">
	      <!--<![endif]-->
	      <param name="quality" value="high" />
	      <param name="wmode" value="transparent" />
	      <param name="swfversion" value="6.0.65.0" />
	      <param name="expressinstall" value="../Scripts/expressInstall.swf" />
	      <!-- El navegador muestra el siguiente contenido alternativo para usuarios con Flash Player 6.0 o versiones anteriores. -->
	      <div>
	        <h4>El contenido de esta página requiere una versión más reciente de Adobe Flash Player.</h4>
	        <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Obtener Adobe Flash Player" width="112" height="33" /></a></p>
          </div>
	      <!--[if !IE]>-->
        </object>
	    <!--<![endif]-->
      </object>
    </div><!--fin 1.2 -->
    
</div>
<!--fin 1 -->



<div id="contenedor_banners"><!-- 2 -->
	
    <div id="contenedor_botonerav"><!-- 2.1 -->
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td><img src="../images/btnv_top.png" width="250" height="10" /></td>
        </tr>
	    <tr>
	      <td><a href="#"><img src="../images/btnv_chat.png" name="chat_online_rent_a_car" width="250" height="73" border="0" id="chat_online_rent_a_car" onmouseover="MM_swapImage('chat_online_rent_a_car','','../images/btnv_chat_ov.png',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        </tr>
	    <tr>
	      <td><a href="#"><img src="../images/btnv_guia.png" name="guia_turismo_guayaquil" width="250" height="74" border="0" id="guia_turismo_guayaquil" onmouseover="MM_swapImage('guia_turismo_guayaquil','','../images/btnv_guia_ov.png',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        </tr>
	    <tr>
	      <td><a href="#"><img src="../images/btnv_cont.png" name="asesoria_serviplatinium_renta_de_autos" width="250" height="76" border="0" id="asesoria_serviplatinium_renta_de_autos" onmouseover="MM_swapImage('asesoria_serviplatinium_renta_de_autos','','../images/btnv_cont_ov.png',1)" onmouseout="MM_swapImgRestore()" /></a></td>
        </tr>
	    <tr>
	      <td><img src="../images/btnv_bottom.png" width="250" height="17" /></td>
        </tr>
      </table>
    </div><!--fin 1.1 -->

	<div id="contenedor_promo"><!-- 2.2 -->
	  <div id="featured">
      
      <a href="../autos.php" target="_blank">
      <img src="../admin/site/banner/img_chevrolet_aveo_activo.jpg" alt="Link" border="0" rel="chiefCaption" />
      </a> 
      
      <a href="../autos.php" target="_blank">
      <img src="../admin/site/banner/img_chevrolet_aveo_family.jpg" alt="Link" border="0" rel="ezioCaption" />
      </a> 
      
      <a href="../autos.php" target="_blank">
      <img src="../admin/site/banner/img_kia_xcite.jpg" alt="Link" border="0" rel="marcusCaption" />
      </a> 
      
      <a href="../autos.php" target="_blank">
      <img src="../admin/site/banner/img_mazda_4x2.jpg" alt="Link" border="0" rel="marcusCaption" />
      </a> 
      
      <a href="../autos.php" target="_blank">
      <img src="../admin/site/banner/img_chevrolet_dmax.jpg" alt="Link" border="0" rel="marcusCaption" />
      
      <a href="../autos.php" target="_blank">
      <img src="../admin/site/banner/img_toyota_hilux.jpg" alt="Link" border="0" rel="marcusCaption" />
	  </a> 
      
      </div>
	  <span class="orbit-caption" id="chiefCaption">new text here</span>
	    <span class="orbit-caption" id="ezioCaption">This is an <em>awesome caption</em> for Ezio. <strong>Note:</strong> This whole image is linked</span>
	    <span class="orbit-caption" id="marcusCaption">This is an <em>awesome caption</em> for Marcus with a <a href="http://www.zurb.com/playground" target="_blank" style="color: #fff">link</a></span>
    </div><!--fin 2.2 -->
    
</div>
<!--fin 2 -->





<div id="contenedor_cuerpo_info"><!-- 3 -->

	<div id="contenedor_info_sec_01"><!-- 3.1 -->

		<div id="contenedor_info"><!-- 3.1.1 -->
	    <!-- TemplateBeginEditable name="informacion" -->informacion<!-- TemplateEndEditable --> </div><!--fin 3.1.1 -->

		<div id="contenedor_publicidades"><!-- 3.1.2 --><!--fin 3.1.2.1 --><!--fin 3.1.2.0 --><!--fin 3.1.2.2 -->
		  <!-- TemplateBeginEditable name="publicidades" -->publicidades<!-- TemplateEndEditable -->
		  <!--fin 3.1.2.0 --><!--fin 3.1.2.3 -->
        
      </div><!--fin 3.1.2 -->
            

	</div><!--fin 3.1 -->
    
    
    
    <div id="contenedor_info_sec_02"><!-- 3.2 --><!--fin 3.2.1 -->
        
        <div id="contenedor_minidobleinfo_02"><!-- 3.2.1 -->
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="78%" class="titulo_rojo_promo"><table width="100%" border="0" cellspacing="10" cellpadding="0">
                <tr>
                  <td width="94%" class="subtitulo_rojo">Estamos Ubicados en Guayaquil - Ecuador</td>
                </tr>
              </table></td>
              <td width="22%" rowspan="2" valign="top"><a href="../contactos.php"><img src="../images/img_telefono_alquiler_vehiculos.png" width="178" height="170" border="0" /></a></td>
            </tr>
            <tr>
              <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="15">
                <tr>
                  <td>Av. de las Américas, cdla. Simón Bolivar Mz. 4 S. 25 esquina.<br />
                    Centro de negocios Rentadoras de Guayaquil - Local 5</td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><table width="100%" border="0" cellspacing="5" cellpadding="0">
                        <tr class="subtitulo_rojo">
                          <td><img src="../images/vineta_rj.png" width="10" height="10" /></td>
                          <td class="subtitulo_negro">(593-4) 2693447</td>
                        </tr>
                      </table></td>
                      <td><table width="100%" border="0" cellspacing="5" cellpadding="0">
                        <tr>
                          <td><img src="../images/vineta_rj.png" width="10" height="10" /></td>
                          <td class="subtitulo_negro">(593-4) 2693447</td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td><table width="100%" border="0" cellspacing="5" cellpadding="0">
                        <tr>
                          <td><img src="../images/vineta_rj.png" width="10" height="10" /></td>
                          <td class="subtitulo_negro">(593-4) 2693447</td>
                        </tr>
                      </table></td>
                      <td><table width="100%" border="0" cellspacing="5" cellpadding="0">
                        <tr>
                          <td><img src="../images/vineta_rj.png" width="10" height="10" /></td>
                          <td class="subtitulo_negro">(593-4) 2693447</td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
            </tr>
          </table>
    </div>
        <!--fin 3.2.1 --><!--fin 3.2.1 -->
        
        
        
        <div id="contenedor_miniinfo_03"><!-- 3.2.3 -->
          <table width="100%" border="0" cellspacing="10" cellpadding="0">
            <tr>
              <td class="titulo_rojo">Formas <span class="titulo_darkgris"> de Pago</span></td>
            </tr>
            <tr>
              <td class="texto_mini_gris"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="10">
                <tr>
                  <td><span class="texto_negro">Aceptamos las siguientes tarjetas de crédito...</span></td>
                </tr>
                <tr>
                  <td><table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="center" class="texto_mini_gris"><img src="../images/logo_visa.gif" width="50" height="30" /></td>
                      <td align="center" class="texto_mini_gris"><img src="../images/logo_mastercard.gif" width="50" height="30" /></td>
                      <td align="center" class="texto_mini_gris"><img src="../images/logo_amex.gif" width="50" height="30" /></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td>O si lo prefiere mediante deposito bancario en cualquier agencia del banco de Guayaquil.</td>
                </tr>
              </table></td>
            </tr>
          </table>
        </div><!--fin 3.2.3 -->
        
    </div><!--fin 3.2 --><!--fin 3.3 -->
    
</div><!--fin 3 -->

<div id="contenedor_pie"><!-- 4 -->
    
    	<div id="contenedor_derechos"><!-- 4.1 -->
    	  <table width="450" border="0" cellpadding="0" cellspacing="5">
    	    <tr>
    	      <td width="104" class="texto_pie_pagina"><p><a href="http://www.xtudiocreativo.com"><img src="../images/logo_xtudiocreativo.png" name="xtudiocreativo_publicidad_ecuador" width="100" height="17" border="0" id="xtudiocreativo_publicidad_ecuador" /></a></p></td>
    	      <td width="16" align="center" class="texto_pie_pagina"><img src="../images/separador.png" width="2" height="11" /></td>
    	      <td width="310" class="texto_pie_pagina"><span class="texto_mini_gris">Sitio web Serviplatinium rent a car<br />
   	          Dise&ntilde;ado y producido por</span> <a href="http://www.xtudiocreativo.com" target="_blank" class="enlaces_mini_gris">Xtudiocreativo</a> <span class="texto_mini_gris">/ 2010</span></td>
  	      </tr>
  	    </table>
   	</div><!--fin 4.1 -->
        
    	<div id="contenedor_enlaces_pie"><!-- 4.2 -->
    	  <table width="100%" border="0" cellspacing="15" cellpadding="0">
    	    <tr>
    	      <td class="texto_mini_gris"><a href="../index.php" class="enlaces_mini_gris">Inicio</a></td>
    	      <td><img src="../images/separador.png" name="alquier" width="2" height="11" id="alquier" /></td>
    	      <td><a href="../vehiculos_de_alquiler_en_promociones.php" class="enlaces_mini_gris">Promociones</a></td>
    	      <td><img src="../images/separador.png" name="autos" width="2" height="11" id="autos" /></td>
    	      <td><a href="../autos.php" class="enlaces_mini_gris">Vehículos</a></td>
    	      <td><img src="../images/separador.png" name="renta_autos" width="2" height="11" id="renta_autos" /></td>
    	      <td><a href="../contactosr.php" class="enlaces_mini_gris">Contáctenos</a></td>
  	      </tr>
  	    </table>
    	</div><!--fin 4.2 -->
    
  </div><!--fin 4 -->


</div>
<!--fin 0 -->




<script type="text/javascript">
<!--
swfobject.registerObject("FlashID");
//-->
</script>
</body>
</html>